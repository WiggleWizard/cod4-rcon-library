package com.zinglish.cod4;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * This library will handle any awkward UDP protocols as well as silly responses from the server
 * such as the print command from clients/server side commands.
 * 
 * Use raw command at your own discretion, only for pros or if there is a command/feature that's
 * not in the library.
 * 
 * Contact me at zinglish[at]gmail.com if you want more features. I was thinking of creating an
 * entire library to handle CoD4 server side things like map objects, player building objects etc,
 * basically just making it easier to make an addon for CoD4 servers.
 * 
 * @author Zinglish
 * @version 0.2
 */
public class CoD4RCON
{
	private String password;
    private String serverHostname;
    private int dstPort;
    
    private DatagramSocket ds;
    private DatagramPacket dp;
    private InetAddress ia;
	
    /**
     * Constructor
     * 
     * @param dstHost Server's IP
     * @param srcPort Local port
     * @param dstPort Server's Port
     * @param RCON Password
     */
	public CoD4RCON(String dstHost, int srcPort, int dstPort, String password)
	{
		this.password = password;
		this.dstPort = dstPort;
		this.serverHostname = dstHost;
		
		try 
		{
			ia = InetAddress.getByName(serverHostname);
		}
		catch (UnknownHostException e) 
		{
			e.printStackTrace();
		}
		
		// Construct the Datagram
		try
		{
			this.ds = new DatagramSocket(srcPort, ia);
		} 
		catch (SocketException e1)
		{
			e1.printStackTrace();
		}
	}
	
	/**
	 * Requests status from server
	 * 
	 * @return Raw print from server
	 */
	public String getStatus()
	{
		return send("status", true);
	}
	
	/**
	 * Gets the map rotation set in the config
	 * 
	 * @return String
	 */
	public String getMapList()
	{
		return send("sv_maprotation", true);
	}

	/**
	 * Gets the current map rotation of the server. This map rotation can be used to determine the next
	 * map by reading the first map in the string as this map rotation starts with the map that is next in
	 * the rotation
	 * 
	 * @return Truncated map rotation
	 */
	public String getCurrentMapRotation()
	{
		return send("sv_maprotationcurrent", true);
	}
	
	/**
	 * Loads the next map
	 */
	public void nextMap()
	{
		send("map_rotate", false);
	}
	
	/**
	 * Changes the current map
	 * 
	 * @param mapName Be sure to use the map's full name here, eg: "mp_backlot"
	 */
	public void setMap(String mapName)
	{
		send("map\r" + mapName, false);
	}
	
	/**
	 * Broadcasts a string to all clients in the chat box (Non modded servers will print "Console:" before the string)
	 * 
	 * @param message The message to be broadcasted
	 */
	public void say(String message)
	{
		send("say\r" + message, false);
	}
	
	/**
	 * Sends a raw string to the server
	 * 
	 * @param command Raw string
	 */
	public void raw(String command)
	{
		send(command, false);
	}
	
	/**
	 * Sends a raw string to the server and returns the response. Function is useful for when you have an
	 * injector for the server and you would like to execute any custom commands
	 * 
	 * @param command Raw string
	 * @return Server response
	 */
	public String getRaw(String command)
	{
		return send(command, true);
	}
	
	private String send(String command, boolean reply)
	{		
		String out = "xxxx" + "rcon\r" + password + "\r" + command + "\0";
		byte[] buff = out.getBytes();
		// Adjust the first 4 bytes to conform to the protocol of CoD4 RCON
		buff[0] = (byte)0xff;
		buff[1] = (byte)0xff;
		buff[2] = (byte)0xff;
		buff[3] = (byte)0xff;
		
		dp = new DatagramPacket(buff, buff.length, ia, dstPort);
		try 
		{
			this.ds.send(dp);
			if(reply)
			{
				return reply();
			}
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		ds.close();

		return null;
	}
	
	private String reply()
	{
		byte[] buff = new byte[2048];
		
		try 
		{
			ds.setSoTimeout(20);
		} 
		catch (SocketException e1) {}
		
		DatagramPacket packet = new DatagramPacket(buff, buff.length);
		String received = "";
		String data = "";
		try
		{
			while(received != null)
			{
				ds.receive(packet);
				received = new String(packet.getData(), 0, packet.getLength());
				
				if(received.length() > 10)
				{
					received = received.replace("print\n", ""); // Remove client command
					received = received.substring(4); // Remove unprintable Chars in the rcon return
				}
				
				data = data + received;
			}
			
		} 
		catch (IOException e) 
		{
			//e.printStackTrace();
		}
		
		return data;
	}
}
