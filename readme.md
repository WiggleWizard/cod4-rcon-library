#CoD4 RCON Library

##What is this?
CoD4 RCON Library is meant to be the easy library for coding CoD4 RCON tools.

It takes care of many things for you, such as handling CoD4 RCON protocols and cleans responses from the server. 

##What functions are exposed?
There are numerous functions, some of which are simple commands and some return data. Here are the functions that return data:
``` java
public String getStatus();              // status
public String getMapList();             // sv_maprotation
public String getCurrentMapRotation();  // sv_maprotationcurrent
public String getRaw(String command);
```
The follow functions exposed are simply commands for the server and do not return any data:
``` java
public void nextMap();                  // map_rotate
public void say(String message);        // say
public void setMap(String mapName);     // map
public void raw(String command);
```

##Examples
CoD4 RCON Library is pretty straight forward, to get the status of the server for instance:
``` java
CoD4RCON localRCON = new CoD4RCON("localhost", 9999, 28960, "password");
System.out.println(localRCON.getStatus());
```

To change the map:
``` java
CoD4RCON localRCON = new CoD4RCON("localhost", 9999, 28960, "password");
localRCON.setMap("mp_backlot");
```
